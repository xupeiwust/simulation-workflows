Steps for reconstructing test case #2

Grid
* Lower Left Corner to -3.14, -2.78
* Physical Length to 42.24, 73.37
* Periodic y to Yes
* Maximum Levels 6
* Refinement Ratio General
* Refinement Ratio subgroup: 1, 4
* Refinement Ration subgroup 2, 2

BC
* Upper X enable
* a Coefficients 1.1, 2.2, 3.3, 4.4
* b Coefficients 0.1, 0.2, 0.3, 0.4
* c Coefficients 0, 0, 0, 1

Solver
* End Time 999.9
* Max Timestep Size 0.0007
* Vorticity Relative Thresholds 0.01, 0.04, 0.02

Output
* Vorticity Off
* Restart Output On
* Restart Dump Interval 150
