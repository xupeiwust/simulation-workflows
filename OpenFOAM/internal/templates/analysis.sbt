<SMTK_AttributeSystem Version="2">
  <Categories>
    <Cat>Fluid Flow</Cat>
  </Categories>
  <!-- Attribute Definitions-->
  <!-- Attribute Definitions-->
  <Definitions>
    <!-- attributes for controlDict -->
    <AttDef Type="application" Label="OpenFOAM Application" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <String Name="application" Label="OpenFOAM Application">
          <ChildrenDefinitions>
            <Group Name="simpleFoam" Label="simpleFoam" />
            <Group Name="icoFoam" Label="icoFoam" />
            <Group Name="interFoam" Label="interFoam" />
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="simpleFoam">simpleFoam</Value>
            </Structure>
            <Structure>
              <Value Enum="icoFoam">icoFoam</Value>
            </Structure>
            <Structure>
              <Value Enum="interFoam">interFoam</Value>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="timeControl" Label="Time Control" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <String Name="start" Label="Start From">
          <ChildrenDefinitions>
            <Group Name="firstTime" Label="First Time" />
            <Group Name="startTime" Label="Start Time" >
              <ItemDefinitions>
                <Double Name="time" Label="Time">
                  <DefaultValue>0.</DefaultValue>
                </Double>
              </ItemDefinitions>
            </Group>
            <Group Name="latestTime" Label="Latest Time" >
              <ItemDefinitions>
                <Double Name="time" Label="Time">
                  <DefaultValue>0.</DefaultValue>
                </Double>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="First Time">firstTime</Value>
            </Structure>
            <Structure>
              <Value Enum="Start Time">startTime</Value>
              <Items>
                <Item>startTime</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Latest Time">latestTime</Value>
              <Items>
                <Item>startTime</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
        <String Name="stop" Label="Stop At">
          <ChildrenDefinitions>
            <Group Name="endTime" Label="End Time" >
              <ItemDefinitions>
                <Double Name="time" Label="Time">
                  <DefaultValue>500</DefaultValue>
                </Double>
              </ItemDefinitions>
            </Group>
            <Group Name="writeNow" Label="Write Now" />
            <Group Name="noWriteNow" Label="No Write Now" />
            <Group Name="nextWrite" Label="Next Write" />
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="End Time">endTime</Value>
              <Items>
                <Item>endTime</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Write Now">writeNow</Value>
            </Structure>
            <Structure>
              <Value Enum="No Write Now">noWriteNow</Value>
            </Structure>
            <Structure>
              <Value Enum="Write Next">writeNext</Value>
            </Structure>
          </DiscreteInfo>
        </String>
        <Double Name="deltaT" Label="Time Step">
          <DefaultValue>1</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="dataWriting" Label="Data Writing" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <String Name="writeControl" Label="Write Control">
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="Time Step">timeStep</Value>
            </Structure>
            <Structure>
              <Value Enum="Run Time">runTime</Value>
            </Structure>
            <Structure>
              <Value Enum="Adjustable Run Time">adjustableRunTime</Value>
            </Structure>
            <Structure>
              <Value Enum="CPU Run Time">cpuTime</Value>
            </Structure>
            <Structure>
              <Value Enum="Clock Time">clockTime</Value>
            </Structure>
          </DiscreteInfo>
        </String>
        <Double Name="writeInterval" Label="Write Interval">
          <DefaultValue>100</DefaultValue>
        </Double>
        <Int Name="purgeWrite" Label="Purge Write" Optional="true" IsEnabledByDefault="false">
          <DefaultValue>0</DefaultValue>
        </Int>
        <String Name="writeFormat" Label="Write ASCII">
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="Binary">binary</Value>
            </Structure>
            <Structure>
              <Value Enum="ASCII">ascii</Value>
            </Structure>
          </DiscreteInfo>
        </String>
        <Int Name="writePrecision" Label="Write Precision" >
          <DefaultValue>6</DefaultValue>
        </Int>
        <Void Name="writeCompression" Label="Compress Files" Optional="true" IsEnabledByDefault="false" />
        <String Name="timeFormat" Label="Time Format">
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="Fixed">fixed</Value>
            </Structure>
            <Structure>
              <Value Enum="Scientific">scientific</Value>
            </Structure>
            <Structure>
              <Value Enum="General">general</Value>
            </Structure>
          </DiscreteInfo>
        </String>
        <Int Name="timePrecision" Label="Time Precision" >
          <DefaultValue>6</DefaultValue>
        </Int>
        <String Name="graphFormat" Label="Graph Format">
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="Raw">raw</Value>
            </Structure>
            <Structure>
              <Value Enum="Gnuplot">gnuplot</Value>
            </Structure>
            <Structure>
              <Value Enum="Grace/xmgr">xmgr</Value>
            </Structure>
            <Structure>
              <Value Enum="jPlot">jplot</Value>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="otherControl" Label="Other Control" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Void Name="adjustTimeStep" Label="Adjust Time Step" Optional="true" IsEnabledByDefault="false" />
        <Double Name="maxCo" Label="Maximum Courant Number">
          <DefaultValue>.5</DefaultValue>
        </Double>
        <Void Name="runTimeModifiable" Label="Runtime Modifiable" Optional="true" IsEnabledByDefault="false" />
        <!-- skipping libs and functions for now -->
      </ItemDefinitions>
    </AttDef>





    <!-- attributes for fvSchemes. see https://cfd.direct/openfoam/user-guide/fvschemes/ -->
    <AttDef Type="ddtSchemes" Label="Time-Stepping Schemes" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <String Name="ddtScheme" Label="Time-Stepping Scheme" Optional="true"> <!-- disabled corresponds to none option -->
          <ChildrenDefinitions>
            <Group Name="CrankNicolson" Label="Crank Nicolson" >
              <ItemDefinitions>
                <Double Name="psi" Label="Off-centering coefficient" AdvanceLevel="1" >
                  <DefaultValue>0.9</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">0</Min>
                    <Max Inclusive="true">1</Max>
                  </RangeInfo>
                </Double>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Value Enum="Backward">backward</Value>
            <Structure>
              <Value Enum="Crank Nicolson">CrankNicolson</Value>
              <Items>
                <Item>CrankNicolson</Item>
              </Items>
            </Structure>
            <Value Enum="Euler">Euler</Value>
            <Value Enum="Local Euler">localEuler</Value>
            <Value Enum="Steady State">steadyState</Value>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="gradSchemes" Label="Gradient Schemes" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <String Name="gradScheme" Label="Gradient Scheme">
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="Gauss Linear">Gauss linear</Value>
            <Value Enum="Least Squares">leastSquares</Value>
            <Value Enum="Gauss cubic">Gauss Cubic</Value>
          </DiscreteInfo>
        </String>
        <Void Name="grad(U)" Label="Use Cell Limited Grad(U)" Optional="true" IsEnabledByDefault="false" AdvanceLevel="1">
          <BriefDescription>Override gradient of velocity</BriefDescription>
        </Void>
        <!-- don't expose these since they are likely linked to the turbulence model
             <Void Name="grad(k)" Label="Use Cell Limited Grad(k)" Optional="true" IsEnabledByDefault="false">
             <BriefDescription>Override gradient of turbulence parameter k</BriefDescription>
             </Void>
             <Void Name="grad(U)" Label="Use Cell Limited Grad(epsilon)" Optional="true" IsEnabledByDefault="false">
             <BriefDescription>Override gradient of turbulence parameter epsilon</BriefDescription>
             </Void>
        -->
      </ItemDefinitions>
    </AttDef>

    <!-- attributes for fvSolution. see https://cfd.direct/openfoam/user-guide/fvsolution/ -->
    <AttDef Type="psolver" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <String Name="solver" Label="Pressure Solver">
          <ChildrenDefinitions>
            <Group Name="pcg" Label="Preconditioned Conjugate-Gradient">
              <ItemDefinitions>
                <String Name="preconditioner" Label="Preconditioner">
                  <DiscreteInfo DefaultIndex="0" Optional="true">
                    <Value Enum="Diagonal Incomplete Cholesky">DIC</Value>
                    <Value Enum="Caching Diagonal Incomplete Cholesky">FDIC</Value>
                    <Value Enum="Diagonal">diagonal</Value>
                    <Value Enum="Geometric-Algebraic Multi-Grid">GAMG</Value>
                  </DiscreteInfo>
                </String>
              </ItemDefinitions>
            </Group>
            <Group Name="smootherOptions" Label="Smoother Options">
              <ItemDefinitions>
                <String Name="smoother" Label="Smoother">
                  <DiscreteInfo DefaultIndex="0" Default="0">
<!--                     <Value Enum="Gauss-Seidel">symGaussSeidel</Value> thought it was this instead of GaussSeidel but the motorbike problem has GaussSeidel -->
                    <Value Enum="Gauss-Seidel">GaussSeidel</Value>
                    <Value Enum="Diagonal Incomplete Cholesky">DIC</Value>
                    <Value Enum="Diagonal Incomplete Cholesky with Gauss-Seidel">DICGaussSeidel</Value>
                  </DiscreteInfo>
                </String>
                <Int Name="nSweeps" Label="Number Of Sweeps">
                  <DefaultValue>1</DefaultValue>
                </Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="1">
            <Structure>
              <Value Enum="Preconditioned Conjugate-Gradient">pcg</Value>
              <Items>
                <Item>pcg</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Generalized Geometric-Algebraic Multi-Grid">GAMG</Value>
              <Items>
                <Item>smootherOptions</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Smoother">smoother</Value>
              <Items>
                <Item>smootherOptions</Item>
              </Items>
            </Structure>
            <Value Enum="Diagonal Solver">diagonal</Value>
          </DiscreteInfo>
        </String>
        <Double Name="tolerance" Label="Tolerance">
          <DefaultValue>1.e-07</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
            <Max Inclusive="true">.1</Max>
          </RangeInfo>
        </Double>
        <Double Name="relTol" Label="Relative Tolerance">
          <DefaultValue>.01</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
            <Max Inclusive="true">.1</Max>
          </RangeInfo>
        </Double>
      </ItemDefinitions>
      </AttDef>

    <!-- attributes for fvSolution. see https://cfd.direct/openfoam/user-guide/fvsolution/ -->
    <AttDef Type="usolver" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <String Name="solver" Label="Velocity Solver">
          <ChildrenDefinitions>
            <Group Name="pbicg" Label="Preconditioned Bi-Conjugate Gradient">
              <ItemDefinitions>
                <String Name="preconditioner" Label="Preconditioner">
                  <DiscreteInfo DefaultIndex="0" Optional="true">
                    <Value Enum="Diagonal Incomplete LU">DILU</Value>
                    <Value Enum="Caching Diagonal Incomplete LU">FDILU</Value>
                    <Value Enum="Diagonal">diagonal</Value>
                    <Value Enum="Geometric-Algebraic Multi-Grid">GAMG</Value>
                  </DiscreteInfo>
                </String>
              </ItemDefinitions>
            </Group>
            <Group Name="smootherOptions" Label="Smoother Options">
              <ItemDefinitions>
                <String Name="smoothSolver" Label="Smoother">
                  <DiscreteInfo DefaultIndex="0" Default="0">
                    <Value Enum="Gauss-Seidel">GaussSeidel</Value>
                    <Value Enum="Diagonal Incomplete LU">DILU</Value>
                    <Value Enum="Diagonal Incomplete LU with Gauss-Seidel">DICGaussSeidel</Value>
                  </DiscreteInfo>
                </String>
                <Int Name="nSweeps" Label="Number Of Sweeps">
                  <DefaultValue>1</DefaultValue>
                </Int>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="Preconditioned Bi-Conjugate-Gradient">PBiCG</Value>
              <Items>
                <Item>pbicg</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Generalized Geometric-Algebraic Multi-Grid">GAMG</Value>
              <Items>
                <Item>smootherOptions</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Smoother">smoothSolver</Value>
              <Items>
                <Item>smootherOptions</Item>
              </Items>
            </Structure>
            <Value Enum="Diagonal Solver">diagonal</Value>
          </DiscreteInfo>
        </String>
        <Double Name="tolerance" Label="Tolerance">
          <DefaultValue>1.e-07</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
            <Max Inclusive="true">.1</Max>
          </RangeInfo>
        </Double>
        <Double Name="relTol" Label="Relative Tolerance">
          <DefaultValue>.01</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
            <Max Inclusive="true">.1</Max>
          </RangeInfo>
        </Double>
      </ItemDefinitions>
    </AttDef>


    <!-- initial conditions -->
    <AttDef Type="initialCondition" Label="Initial Condition" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Double Name="pressureIC" Label="Initial Pressure" >
          <DefaultValue>0.0</DefaultValue>
        </Double>
        <Double Name="velocityIC" Label="Initial Velocity" NumberOfRequiredValues="3">
          <ComponentLabels>
            <Label>u</Label>
            <Label>v</Label>
            <Label>w</Label>
          </ComponentLabels>
          <DefaultValue>0.0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>

  </Definitions>

</SMTK_AttributeSystem>
