# ACE3P

The ACE3P workflow files have been moved to https://gitlab.kitware.com/cmb/plugins/ace3p-extensions

ACE3P is the Advanced Computational Electromagnetic Simulation Suite,
developed and maintained by the SLAC National Accelerator Laboratory.

More information on ACE3P is at
https://confluence.slac.stanford.edu/display/AdvComp/ACE3P+-+Advanced+Computational+Electromagnetic+Simulation+Suite
